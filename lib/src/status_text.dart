import 'package:flutter/material.dart';

class StatusText extends StatelessWidget {
  final bool statusReady;

  StatusText({@required this.statusReady});

  static final _styleReady = const TextStyle(
    color: Colors.green,
    fontSize: 20, /* normal fontSize is 14 */
  );

  static final _styleAbsent = const TextStyle(
    color: Colors.red,
    fontSize: 20,
  );

  /* TODO Write some codes which figures out the current status
      Based on that, this class will build different widgets
   */

  Text _generateText() {
    return statusReady == true
        ? Text(
            'Einsatzbereit',
            style: _styleReady,
          )
        : Text(
            'Abgemeldet',
            style: _styleAbsent,
          );
  }

  Text _generateTime() {
    return statusReady == true ? Text(' ') : Text('bis 11:34 Uhr');
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: <Widget>[
          _generateText(),
          _generateTime(),
        ],
      ),
    );
  }
}
