import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

const double _kPickerSheetHeight = 216.0;
const double _kPickerItemHeight = 32.0;

class ChangeStatus extends StatelessWidget {
  final ValueChanged<bool> onStatusChanged;
  final ValueChanged<Duration> onTimerChanged;
  final bool statusReady;
  final Duration timer;

  ChangeStatus({
    Key key,
    @required this.statusReady,
    @required this.timer,
    @required this.onStatusChanged,
    @required this.onTimerChanged,
  }) : super(key: key);


  void _handleStatus() {
    print('[change_status] _handleStatusChanged called');
    onStatusChanged(!statusReady);
  }

  void _handleTimer(Duration newTimer) {
    print('[change_status] _hadleTimerChanged called');
    onTimerChanged(newTimer);
  }

  Widget _buildTimerPicker(BuildContext context) {
    return FlatButton(
      child: Text(
        '${timer.inHours} Std, '
        '${(timer.inMinutes % 60).toString().padLeft(2, '0')} Min',
        style: TextStyle(
          fontSize: 20,
        ),
      ),
      onPressed: () {
        showCupertinoModalPopup<void>(
          context: context,
          builder: (BuildContext context) {
            return Container(
              height: _kPickerSheetHeight,
              padding: const EdgeInsets.only(top: 6.0),
              color: CupertinoColors.white,
              child: DefaultTextStyle(
                style: const TextStyle(
                  color: CupertinoColors.black,
                  fontSize: 22.0,
                ),
                child: GestureDetector(
                  // Blocks taps from propagating to the modal sheet and popping.
                  onTap: () {},
                  child: SafeArea(
                    top: false,
                    child: CupertinoTimerPicker(
                      minuteInterval: 5,
                      mode: CupertinoTimerPickerMode.hm,
                      initialTimerDuration: timer,
                      onTimerDurationChanged: _handleTimer,
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }

  Widget _buildAbsentButton(BuildContext context) {
    return RaisedButton(
      onPressed: _handleStatus,
      color: statusReady ? Colors.redAccent : Colors.green,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      child: Text(
        statusReady ? 'Abmelden' : "Einsatzbereit",
        style: TextStyle(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return statusReady
        ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildTimerPicker(context),
              _buildAbsentButton(context),
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildAbsentButton(context),
            ],
          );
  }
}
