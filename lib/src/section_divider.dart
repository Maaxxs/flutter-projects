import 'package:flutter/material.dart';


class PageDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Divider(
      color: Colors.grey,
      height: 20,
    );
  }
}
