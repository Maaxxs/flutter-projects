import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'src/absent_card.dart';
import 'src/section_divider.dart';
import 'src/status_text.dart';
import 'src/change_status.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
//      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _styleHeader = const TextStyle(
    color: Colors.black,
    fontSize: 16,
  );

  var _statusReady = true;
  var _timer = Duration(hours: 1, minutes: 0);

  void _handleStatusChanged(bool newValue) {
    print('[main] _hadleStatusChanged called');
    setState(() {
      _statusReady = newValue;
    });
  }

  void _handleTimerChanged(Duration newTimer) {
    print('[main] _hadleTimerChanged called');
    setState(() {
      _timer = newTimer;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text('FF Albstadt-Ebingen'),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      'Aktueller Status:',
                      style: _styleHeader,
                    ),
                    Text(' '),
                  ],
                ),
                StatusText(
                  statusReady: _statusReady,
                ),
              ],
            ),
            PageDivider(),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Text(
                'Geplante Abwesenheiten',
                style: _styleHeader,
              ),
            ),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  AbsentCard(),
                  AbsentCard(),
                  AbsentCard(),
                ],
              ),
            ),
            PageDivider(),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Text(
                'Sofortiger Statuswechsel',
                style: _styleHeader,
              ),
            ),
            ChangeStatus(
              timer: _timer,
              statusReady: _statusReady,
              onStatusChanged: _handleStatusChanged,
              onTimerChanged: _handleTimerChanged,
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.remove_red_eye),
          title: Text('Übersicht'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.calendar_today),
          title: Text('Planen'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.settings),
          title: Text('Einstellungen'),
        ),
      ]),
    );
  }
}
